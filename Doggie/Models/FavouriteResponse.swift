//
//  FavouriteResponse.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 27/05/2023.
//

struct FavouriteResponse: Decodable {
    let id: Int
}

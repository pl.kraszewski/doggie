//
//  Favourite.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 26/05/2023.
//

struct Favourite: Decodable {
    var id: Int
    let imageId: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case imageId = "image_id"
    }
}

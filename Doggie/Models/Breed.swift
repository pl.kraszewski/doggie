//
//  Breed.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

struct Breed: Decodable {
    let name: String
    let breedFor: String?
    let breedGroup: String?
    let lifeSpan: String?
    let temperament: String?
    let origin: String?
    let image: BreedImage?
    
    enum CodingKeys: String, CodingKey {
        case name, temperament, origin, image
        case breedFor = "bred_for"
        case breedGroup = "breed_group"
        case lifeSpan = "life_span"
    }
}

struct BreedImage: Decodable {
    let id: String
    let url: String
}


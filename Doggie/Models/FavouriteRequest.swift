//
//  FavoriteRequest.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 26/05/2023.
//

struct FavouriteRequest: Encodable {
    let imageId: String
    
    enum CodingKeys: String, CodingKey {
        case imageId = "image_id"
    }
}

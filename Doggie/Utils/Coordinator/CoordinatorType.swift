//
//  CoordinatorType.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

import Foundation

protocol CoordinatorType {
    func start()
}

extension CoordinatorType where Self: BaseCoordinator {
    func finish() {
        parent?.remove(child: self)
    }
}

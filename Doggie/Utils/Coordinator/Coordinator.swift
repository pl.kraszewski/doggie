//
//  Coordinator.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

typealias Coordinator = BaseCoordinator & CoordinatorType

//
//  BaseCoordinator.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

class BaseCoordinator {
    
    //MARK: - Properties
    
    fileprivate(set) weak var parent: Coordinator?
    private var childrens: [Coordinator] = []
    
    //MARK: - Methods
    
    func add(child coordinator: Coordinator) {
        if !childrens.contains(where: {$0 === coordinator }) {
            childrens.append(coordinator)
        }
    }
    
    func remove(child coordinator: Coordinator) {
        if let index = childrens.firstIndex(where: { $0 === coordinator }) {
            childrens.remove(at: index)
        }
    }
}

//MARK: - Extension

extension CoordinatorType where Self: BaseCoordinator {
    func coordinate(to coordinator: Coordinator) {
        add(child: coordinator)
        coordinator.parent = self
        coordinator.start()
    }
}

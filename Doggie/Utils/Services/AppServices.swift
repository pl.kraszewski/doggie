//
//  AppServices.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

struct AppServices: HasNetworkingService {
    
    // MARK: - Properties
    
    let networkingService: NetworkingServicable
    
    // MARK: - Methods
    
    static func makeAppServices() -> AppServices {
        AppServices(
            networkingService: NetworkingService(
                targerProvider: TargetProvider()
            )
        )
    }
}

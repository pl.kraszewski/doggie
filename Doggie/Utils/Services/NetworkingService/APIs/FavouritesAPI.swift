//
//  FavouritesAPI.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 26/05/2023.
//

struct FavouritesAPI: Targetable {
    
    // MARK: - Endpoint
    enum Endpoint {
        case getFavourites
        case sendFavourites(FavouriteRequest)
        case deleteFavourites(String)
    }
    
    // MARK: - Properties
    
    var host: String { "api.thedogapi.com" }
    var apiVersion: String { "/v1" }
    var authorization: Authorization? {
        .apiKey("live_Fp0X97lPjMKJCfzAmcNAo3jlTTZVvM9yrfkI7buzlSFR9T27FQhhhTUJtt6WMgOB")
    }
    
    var additionalDefaultTargetSetup: ((Target) -> Target)? {
        { $0.header("Content-Type", "application/json") }
    }
    
    private let endpoint: Endpoint
    
    var target: Target {
        switch endpoint {
        case .getFavourites:
            return restrictedDefaultTarget
                .path("/favourites")
                .method(.get)
        case .sendFavourites(let data):
            return restrictedDefaultTarget
                .path("/favourites")
                .method(.post)
                .data(data)
        case .deleteFavourites(let id):
            return restrictedDefaultTarget
                .path("/favourites/\(id)")
                .method(.delete)
        }
    }
    
    // MARK: - Initializator
    
    init(endpoint: Endpoint) {
        self.endpoint = endpoint
    }
}

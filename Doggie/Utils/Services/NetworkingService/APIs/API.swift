//
//  API.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

enum API {
    case breeds(BreedsAPI.Endpoint)
    case favourites(FavouritesAPI.Endpoint)
    
    func makeTargetable() -> Targetable {
        switch self {
        case .breeds(let endpoint):
            return BreedsAPI(endpoint: endpoint)
        case .favourites(let endpoint):
            return FavouritesAPI(endpoint: endpoint)
        }
    }
}

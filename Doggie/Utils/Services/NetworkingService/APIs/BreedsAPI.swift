//
//  BreedsAPI.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

struct BreedsAPI: Targetable {
    
    // MARK: - Endpoint
    enum Endpoint {
        case getBreeds
    }
    
    // MARK: - Properties
    
    var host: String { "api.thedogapi.com" }
    var apiVersion: String { "/v1" }
    var authorization: Authorization? {
        .apiKey("live_Fp0X97lPjMKJCfzAmcNAo3jlTTZVvM9yrfkI7buzlSFR9T27FQhhhTUJtt6WMgOB")
    }
    
    var additionalDefaultTargetSetup: ((Target) -> Target)? {
        { $0.header("Content-Type", "application/json") }
    }
    
    private let endpoint: Endpoint
    
    var target: Target {
        switch endpoint {
        case .getBreeds:
            return restrictedDefaultTarget
                .path("/breeds")
                .method(.get)
        }
    }
    
    // MARK: - Initializator
    
    init(endpoint: Endpoint) {
        self.endpoint = endpoint
    }
}

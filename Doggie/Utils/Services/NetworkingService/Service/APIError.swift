//
//  APIError.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

enum APIError: Error {
    case unableToMakeURL
    case unknownInternal
    case unauthorizedAccess
    case custom(message: String)
    
    var message: String {
        switch self {
        case .unableToMakeURL:
            return "Unable to make URL"
        case .unknownInternal:
            return "Unknown internal Error..."
        case .unauthorizedAccess:
            return "Unauthorized access/ wrong or missing API Key "
        case .custom(let message):
            return message
        }
    }
}

//
//  NetworkingServicable.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

import Combine
import UIKit

protocol HasNetworkingService {
    var networkingService: NetworkingServicable { get }
}

protocol NetworkingServicable {
    func request<T: Decodable>(_ api: API) -> AnyPublisher<T, Error>
    func downloadImage(from url: URL) -> AnyPublisher<UIImage?, Error>
}

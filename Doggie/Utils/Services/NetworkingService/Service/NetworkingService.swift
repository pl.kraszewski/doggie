//
//  NetworkingService.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

import Combine
import UIKit

final class NetworkingService: NetworkingServicable {
    
    // MARK: - Parameters
    
    private let targerProvider: TargetProvider
    private var session = URLSession(configuration: .default)
    
    static let decoder = JSONDecoder()

    // MARK: - Initializator
    
    init(targerProvider: TargetProvider) {
        self.targerProvider = targerProvider
    }

    func request<T>(_ api: API) -> AnyPublisher<T, Error> where T : Decodable {
        let targetable = targerProvider.target(for: api)
        var target = targetable.target
        do {
            let request = try target.makeRequest()
            return session.dataTaskPublisher(for: request)
                .tryMap { data, response in
                    guard let httpResponse = response as? HTTPURLResponse else {
                        throw APIError.unknownInternal
                    }

                    if httpResponse.statusCode == 401 {
                        throw APIError.unauthorizedAccess
                    }
                    
                    if httpResponse.statusCode != 200 {
                        throw APIError.custom(
                            message: "Satus code: \(httpResponse.statusCode)"
                        )
                    }
                    return data
                }
                .decode(type: T.self, decoder: NetworkingService.decoder)
                .receive(on: DispatchQueue.main)
                .eraseToAnyPublisher()
        } catch let error {
            return Fail<T, Error>(error: error)
                .eraseToAnyPublisher()
        }
    }
    
    func downloadImage(from url: URL) -> AnyPublisher<UIImage?, Error>  {
        session.dataTaskPublisher(for: url)
            .map {
                data, _ in UIImage(data: data)
            }
            .mapError { error -> Error in
                return error
            }
            .eraseToAnyPublisher()
    }
}

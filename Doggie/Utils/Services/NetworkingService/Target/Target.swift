//
//  Target.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

import Foundation

struct Target {
    
    // MARK: - Parameters
    
    private var schema: String = "https"
    private let host: String
    private let apiVersion: String
    private(set) var path: String = ""
    private(set) var method: HTTPMethod = .get
    private(set) var parameters: [String: Any]?
    private(set) var headers: [String: String]?
    private(set) var encoding: ParameterEncoding = .queryString
    
    // MARK: - Initializator
    
    init(_ host: String, apiVersion: String) {
        self.host = host
        self.apiVersion = apiVersion
    }
}

// MARK: - Builder Methods

extension Target {
    
    // MARK: Request
    
    mutating func makeRequest() throws -> URLRequest {
        var components = URLComponents()
        components.scheme = schema
        components.host = host
        components.path = apiVersion + path
        
        if case .queryString = encoding, let param = parameters as? [String: String] {
            components.setQueryItems(with: param)
        }
        
        guard let url = components.url else {
            throw APIError.unableToMakeURL
        }
        var request = URLRequest(url: url)
        
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = headers
        
        if case .jsonBody = encoding {
            request.httpBody = try JSONSerialization.data(
                withJSONObject: parameters ?? [:]
            )
        }
        return request
    }
    
    // MARK: Parameters

    func schema(_ value: String) -> Target {
        var newRequest = self
        newRequest.schema = value
        return newRequest
    }
    
    func path(_ value: String) -> Target {
        var newRequest = self
        newRequest.path = value
        return newRequest
    }
    
    func method(_ method: HTTPMethod) -> Target {
        var newRequest = self
        newRequest.method = method
        newRequest.encoding = (method == .get) ? .queryString : .jsonBody
        return newRequest
    }
    
    func parameters(_ parameters: [String: Any?]) -> Target {
        var newRequest = self
        if newRequest.parameters == nil { newRequest.parameters = [:] }
        newRequest.parameters?
            .merge(parameters.compactMapValues { $0 }) { current, _ in current }
        return newRequest
    }
    
    func data<Model: Encodable>(_ data: Model) -> Target {
        var newRequest = self
        
        guard
            let data = try? JSONEncoder().encode(data),
            let dictionary = try? JSONSerialization.jsonObject(with: data) as? [String: Any]
        else { return self }
        
        newRequest.parameters = dictionary
        return newRequest
    }
    
    func header(_ key: String, _ value: String) -> Target {
        var newRequest = self
        if newRequest.headers == nil { newRequest.headers = [:] }
        newRequest.headers?[key] = value
        return newRequest
    }
    
    func encoding(_ encoding: ParameterEncoding) -> Target {
        var newRequest = self
        newRequest.encoding = encoding
        return newRequest
    }
}

//
//  TargetProvidable.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

protocol TargetProvideable {
    func target(for api: API) -> Targetable
}

//
//  TargetProvider.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

struct TargetProvider: TargetProvideable {
    func target(for api: API) -> Targetable {
        api.makeTargetable()
    }
}

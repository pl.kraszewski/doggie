//
//  HTTPMethod.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

struct HTTPMethod: RawRepresentable, Hashable {
    static let get = HTTPMethod(rawValue: "GET")
    static let post = HTTPMethod(rawValue: "POST")
    static let delete = HTTPMethod(rawValue: "DELETE")
    
    let rawValue: String
}

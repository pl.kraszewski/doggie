//
//  Targetable.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

import Foundation

protocol Targetable {
    var apiVersion: String { get }
    var host: String { get }
    var authorization: Authorization? { get }
    var target: Target { get }
    var defaultTarget: Target { get }
    var additionalDefaultTargetSetup: ((Target) -> Target)? { get }
    var restrictedDefaultTarget: Target { get }
}

// MARK: - Default Target Setup

extension Targetable {
    var authorization: Authorization? { nil }
    var additionalDefaultTargetSetup: ((Target) -> Target)? { nil }
    
    var defaultTarget: Target {
        var target = Target(host, apiVersion: apiVersion)
        if let additionalDefaultTargetSetup = additionalDefaultTargetSetup {
            target = additionalDefaultTargetSetup(target)
        }
        return target
    }
    
    var restrictedDefaultTarget: Target {
        switch authorization {
        case .apiKey(let key):
            return defaultTarget.header("x-api-key", key)
        default:
            return defaultTarget
        }
    }
}

//
//  ParameterEncoding.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

enum ParameterEncoding {
    case queryString
    case jsonBody
}

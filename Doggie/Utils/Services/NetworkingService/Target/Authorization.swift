//
//  Authorization.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

enum Authorization {
    case apiKey(String)
}

//
//  BreedListViewController.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

import UIKit
import Combine

final class BreedListViewController: UIViewController {
    
    // MARK: - Properties
    
    weak var coordinator: BreedListCoordinator?
    
    private let viewModel: BreedListViewModel
    private var cancellables = Set<AnyCancellable>()
    
    private var contentView: BreedListView {
        return view as! BreedListView
    }
    
    // MARK: - Initializator
    
    init(services: AppServices) {
        self.viewModel = BreedListViewModel(services)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func loadView() {
        view = BreedListView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        viewModel.getBreeds()
        contentView.activityIndicator.startAnimating()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewModel.getFavourites()
    }
}

// MARK: - Setup

private extension BreedListViewController {
    func setup() {
        setupViewModel()
        setupTableView()
    }
    
    // MARK: - Bindings
    
    func setupTableView() {
        contentView.tableView.delegate = self
        contentView.tableView.dataSource = self
        contentView.tableView.separatorStyle = .none
        
        contentView.tableView.register(
            ImageDescriptionTableViewCell.self,
            forCellReuseIdentifier: ImageDescriptionTableViewCell.identifier
        )
    }
    
    func setupViewModel() {
        viewModel.updateView
            .receive(on: DispatchQueue.main)
            .sink { [weak self] in
                self?.configure()
            }
            .store(in: &cancellables)
        
        viewModel.showAlert
            .receive(on: DispatchQueue.main)
            .sink { [weak self] in
                self?.showAlert(
                    title: $0.title,
                    message: $0.message,
                    actions: $0.actions
                )
            }
            .store(in: &cancellables)
    }
}

// MARK: - Configure

private extension BreedListViewController {
    func configure() {
        contentView.activityIndicator.isHidden = false
        contentView.tableView.reloadData()
        if !viewModel.dataSource.isEmpty {
            contentView.activityIndicator.stopAnimating()
        }
    }
}

// MARK: UITableViewDelegate

extension BreedListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let breed = viewModel.dataSource[indexPath.row]
        let imageID = breed.image?.id
        let image = imageID.flatMap { viewModel.imagesDataSource[$0] }
        let favourite = imageID.flatMap { id in
            viewModel.favouritesDataSource.first { $0.imageId == id }
        }

        coordinator?.runSingleBreedCoordinator(breed, image, favourite)
    }
}

// MARK: UITableViewDataSource

extension BreedListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: ImageDescriptionTableViewCell.identifier,
            for: indexPath
        ) as? ImageDescriptionTableViewCell else { return UITableViewCell() }
        
        let breed = viewModel.dataSource[indexPath.row]
        cell.titleLabel.text = breed.name
        cell.descriptionLabel.text = breed.breedFor ?? R.string.base.defaultBreedFor()
        
        if let imageID = breed.image?.id {
            cell.favouriteImage.image =
            viewModel.favouritesDataSource.contains(where: { $0.imageId == imageID }) ?
            Symbol.fillStar : Symbol.star
            
            if viewModel.imagesDataSource.contains(where: { $0.key == imageID }) {
                cell.mainImage.image = viewModel.imagesDataSource[imageID]
                cell.activityIndicator.isHidden = true
                cell.activityIndicator.stopAnimating()
            } else {
                cell.activityIndicator.isHidden = false
                cell.activityIndicator.startAnimating()
            }
        }
        return cell
    }
}


//
//  BreedListViewModel.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

import UIKit.UIImage
import Combine

final class BreedListViewModel {
    
    typealias Services = HasNetworkingService
    
    // MARK: - Properties
    var dataSource = [Breed]() {
        didSet { updateView.send() }
    }
    
    var imagesDataSource = [String: UIImage]() {
        didSet { updateView.send() }
    }
    
    var favouritesDataSource = [Favourite]() {
        didSet { updateView.send() }
    }
    
    private var services: Services
    private var cancellables = Set<AnyCancellable>()
    
    var updateView = PassthroughSubject<Void, Never>()
    var showAlert = PassthroughSubject<AppAlert, Never>()
    
    // MARK: - Initializator
    
    init(_ services: Services) {
        self.services = services
    }
}

// MARK: - Methods

extension BreedListViewModel {
    func getBreeds() {
        services.networkingService.request(
            .breeds(.getBreeds)
        )
        .sink { [weak self] in
            if case .failure(let error) = $0 {
                guard let self else { return }
                self.showAlert.send(
                    AppAlert(
                        title: R.string.base.alertTitle(),
                        message: R.string.base.alertMessege(error.localizedDescription),
                        actions: [
                            UIAlertAction(
                                title: R.string.base.alertTryAgain(),
                                style: .default,
                                handler: { _ in
                                    self.getBreeds()
                                }
                            )
                        ]
                    )
                )
            }
        } receiveValue: { [weak self] (breeds: [Breed]) in
            guard let self else { return }
            self.dataSource = breeds
            self.getImages(for: breeds)
        }
        .store(in: &cancellables)
    }
    
    func getFavourites() {
        services.networkingService.request(
            .favourites(.getFavourites)
        )
        .sink {
            if case .failure(let error) = $0 {
                print("ERROR: \(error)")
            }
        } receiveValue: { [weak self] (favourites: [Favourite]) in
            self?.favouritesDataSource = favourites
        }
        .store(in: &cancellables)
    }
}

// MARK: - Private Methods

private extension BreedListViewModel {
    func getImages(for breeds: [Breed]) {
        for (index, breed) in breeds.enumerated() {
            guard
                let imageUrl = breed.image?.url,
                let url = URL(string: imageUrl),
                let id = breed.image?.id
            else { return }

            DispatchQueue.main.asyncAfter(
                deadline: .now() + (Constants.fetchDataDuration * Double(index))
            ) { [weak self] in
                self?.getImage(from: url, with: id)
            }
        }
    }
    
    func getImage(from url: URL, with id: String) {
        services.networkingService.downloadImage(from: url)
            .sink {
                if case .failure(let error) = $0 {
                    print("ERROR: \(error)")
                }
            } receiveValue: { [weak self] in
                self?.imagesDataSource[id] = $0
            }
            .store(in: &cancellables)
    }
}

// MARK: - Constants

private extension BreedListViewModel {
    enum Constants {
        static let fetchDataDuration: CGFloat = 0.2
    }
}

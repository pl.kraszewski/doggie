//
//  BreedListView.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

import UIKit

final class BreedListView: UIView {
    
    // MARK: - Private Subview
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = R.string.base.appName()
        label.textAlignment = .center
        label.font = Font.main60
        label.textColor = Color.defaultDark
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: - Subview

    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.layer.cornerRadius = GlobalConstants.cornerRadius
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = .large
        indicator.color = Color.brown
        indicator.translatesAutoresizingMaskIntoConstraints = false
        return indicator
    }()
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupSubview()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Setup

private extension BreedListView {
    func setupView() {
        backgroundColor = Color.beige
    }
    
    func setupSubview() {
        [
            titleLabel,
            tableView,
            activityIndicator
        ]
            .forEach(addSubview)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate(
            [
                titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
                titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: Margins.large),
                
                tableView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Margins.outer),
                tableView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Margins.outer),
                tableView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Margins.outer),
                tableView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Margins.outer),
                
                activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
                activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor)
            ]
        )
    }
}

//
//  BreedListCoordinator.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

import UIKit

final class BreedListCoordinator: Coordinator {
    
    // MARK: - Properties
    
    private let appServices: AppServices
    private let navigationController: UINavigationController
    
    // MARK: - Initialization
    
    init(navigationController: UINavigationController, appServices: AppServices) {
        self.navigationController = navigationController
        self.appServices = appServices
    }
    
    // MARK: - Startable
    
    func start() {
        let viewController = BreedListViewController(services: appServices)
        viewController.coordinator = self
        navigationController.setViewControllers([viewController], animated: false)
    }
}

// MARK: - Methods

extension BreedListCoordinator {
    func runSingleBreedCoordinator(_ breed: Breed,_ image: UIImage?,_ favourite: Favourite?) {
        let coordinator = SingleBreedCoordinator(
            navigationController: navigationController,
            appServices: appServices,
            breed: breed,
            breedImage: image,
            favourite: favourite
        )
        coordinate(to: coordinator)
    }
}

//
//  SingleBreedViewController.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

import UIKit
import Combine

final class SingleBreedViewController: UIViewController {
    
    // MARK: - Properties
    
    private let viewModel: SingleBreedViewModel
    private var cancellables = Set<AnyCancellable>()
    
    private var contentView: SingleBreedView {
        return view as! SingleBreedView
    }
    
    // MARK: - Initializator
    
    init(
        services: AppServices,
        breed: Breed,
        breedImage: UIImage?,
        favourite: Favourite?
    ) {
        self.viewModel = SingleBreedViewModel(
            services,
            breed: breed,
            breedImage: breedImage,
            favourite: favourite
        )
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func loadView() {
        view = SingleBreedView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
}

// MARK: - Setup

private extension SingleBreedViewController {
    private func setup() {
        configure()
        setupAction()
        setupViewModel()
    }
    
    // MARK: - Bindings
    
    func setupAction() {
        contentView.favouriteButton.addTarget(
            self,
            action: #selector(favouriteButtonTapped),
            for: .touchUpInside
        )
    }
    
    func setupViewModel() {
        viewModel.updateView
            .receive(on: DispatchQueue.main)
            .sink { [weak self] in
                guard let self else { return }
                self.configure()
                self.contentView.activityIndicator.stopAnimating()
                self.contentView.activityIndicator.isHidden = true
                self.contentView.favouriteButton.isHidden = false
            }
            .store(in: &cancellables)
    }
}

// MARK: - Private Methods

private extension SingleBreedViewController {
    @objc private func favouriteButtonTapped() {
        contentView.activityIndicator.startAnimating()
        contentView.activityIndicator.isHidden = false
        contentView.favouriteButton.isHidden = true
        
        viewModel.isFavourite ?
        viewModel.deleteFavourite() : viewModel.saveAsFavourite()
    }
}

// MARK: - Configure

private extension SingleBreedViewController {
    func configure() {
        contentView.mainImage.image = viewModel.mainImage
        contentView.breedNameLabel.text = viewModel.nameTitle
        
        contentView.detailsView.breedForInfoLabel.text = viewModel.breedForInfo
        contentView.detailsView.breedGroupInfoLabel.text = viewModel.breedGroupInfo
        contentView.detailsView.lifeSpanInfoLabel.text = viewModel.lifeSpanInfo
        contentView.detailsView.temperamentInfoLabel.text = viewModel.temperamentInfo
        contentView.detailsView.originInfoLabel.text = viewModel.originInfo
        
        contentView.favouriteButton.setBackgroundImage(
            viewModel.isFavourite ? Symbol.fillStar : Symbol.star, for: .normal
        )
    }
}

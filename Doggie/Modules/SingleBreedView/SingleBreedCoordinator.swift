//
//  SingleBreedCoordinator.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

import UIKit
import Combine

final class SingleBreedCoordinator: Coordinator {
    
    // MARK: - Properties
    
    private let appServices: AppServices
    private let breed: Breed
    private let breedImage: UIImage?
    private let favourite: Favourite?
    private let navigationController: UINavigationController
    private var cancellables = Set<AnyCancellable>()
    
    // MARK: - Initialization
    
    init(
        navigationController: UINavigationController,
        appServices: AppServices,
        breed: Breed,
        breedImage: UIImage?,
        favourite: Favourite?
    ) {
        self.navigationController = navigationController
        self.appServices = appServices
        self.breed = breed
        self.breedImage = breedImage
        self.favourite = favourite
    }
    
    // MARK: - Startable
    
    func start() {
        let viewController = SingleBreedViewController(
            services: appServices,
            breed: breed,
            breedImage: breedImage,
            favourite: favourite
        )
        navigationController.pushViewController(viewController, animated: true)
    }
}

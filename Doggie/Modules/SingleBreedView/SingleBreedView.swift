//
//  SingleBreedView.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

import UIKit

final class SingleBreedView: UIView {
    
    // MARK: - Private Subview
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = R.string.base.appName()
        label.textAlignment = .center
        label.font = Font.main60
        label.textColor = Color.defaultDark
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = Color.transparentWhite
        scrollView.layer.cornerRadius = GlobalConstants.cornerRadius
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    // MARK: - Subview
    
    let mainImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    var favouriteButton: UIButton = {
        let button = UIButton()
        button.tintColor = Color.brown
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let breedNameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = Font.main40
        label.textColor = Color.brown
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var detailsView: DetailsView = {
        let view = DetailsView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = .large
        indicator.color = Color.brown
        indicator.translatesAutoresizingMaskIntoConstraints = false
        return indicator
    }()
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupScrollView()
        setupSubview()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Setup

private extension SingleBreedView {
    func setupView() {
        backgroundColor = Color.beige
    }
    
    func setupScrollView() {
        scrollView.addSubview(detailsView)
    }
    
    func setupSubview() {
        [
            titleLabel,
            mainImage,
            favouriteButton,
            activityIndicator,
            breedNameLabel,
            scrollView
        ]
            .forEach(addSubview)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate(
            [
                titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
                titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: Margins.large),
                
                mainImage.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Margins.outer),
                mainImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Margins.outer),
                mainImage.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Margins.outer),
                mainImage.heightAnchor.constraint(equalTo: mainImage.widthAnchor, multiplier: Constants.imageMultiplier),
                
                favouriteButton.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
                favouriteButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Margins.outer),
                favouriteButton.heightAnchor.constraint(equalToConstant: Constants.buttonHeight),
                favouriteButton.widthAnchor.constraint(equalTo: favouriteButton.heightAnchor, multiplier: Constants.buttonMultiplier),
                
                activityIndicator.centerXAnchor.constraint(equalTo: favouriteButton.centerXAnchor),
                activityIndicator.centerYAnchor.constraint(equalTo: favouriteButton.centerYAnchor),
                
                breedNameLabel.topAnchor.constraint(equalTo: mainImage.bottomAnchor, constant: Margins.small),
                breedNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Margins.outer),
                breedNameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Margins.outer),
                
                scrollView.topAnchor.constraint(equalTo: breedNameLabel.bottomAnchor, constant: Margins.outer),
                scrollView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Margins.outer),
                scrollView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Margins.outer),
                scrollView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Margins.outer),
                
                detailsView.topAnchor.constraint(equalTo: scrollView.topAnchor),
                detailsView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
                detailsView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
                detailsView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                detailsView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
            ]
        )
    }
}

// MARK: - Constants

extension SingleBreedView {
    enum Constants {
        static let imageMultiplier: CGFloat = 1/1.5
        static let buttonMultiplier: CGFloat = 1
        static let buttonHeight: CGFloat = 50.0
    }
}

//
//  SingleBreedViewModel.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

import UIKit.UIImage
import Combine

final class SingleBreedViewModel {
    
    typealias Services = HasNetworkingService
    
    //MARK: - Properties
    
    var mainImage: UIImage? { breedImage }
    var nameTitle: String { breed.name }
    var breedForInfo: String? { breed.breedFor ?? R.string.base.defaultBreedFor() }
    var breedGroupInfo: String? { breed.breedGroup ?? R.string.base.defaultBreedGroup() }
    var lifeSpanInfo: String? { breed.lifeSpan ?? R.string.base.defaultLifeSpan() }
    var temperamentInfo: String? { breed.temperament ?? R.string.base.defaultTemperament() }
    var originInfo: String? { breed.origin ?? R.string.base.defaultOrigin() }
    var initialFavourite: Bool { favourite != nil }
    
    var isFavourite: Bool {
        didSet { updateView.send() }
    }
    
    private var favourite: Favourite?
    private let breed: Breed
    private let breedImage: UIImage?
    
    private var services: Services
    private var cancellables = Set<AnyCancellable>()
    
    var updateView = PassthroughSubject<Void, Never>()
    
    //MARK: - Initializator
    
    init(
        _ services: Services,
        breed: Breed,
        breedImage: UIImage?,
        favourite: Favourite?
    ) {
        self.services = services
        self.breed = breed
        self.breedImage = breedImage
        self.favourite = favourite
        self.isFavourite = favourite != nil
    }
}

//MARK: - Methods

extension SingleBreedViewModel {
    func saveAsFavourite() {
        guard let imageId = breed.image?.id else {
            isFavourite = false
            return
        }
        
        services.networkingService.request(
            .favourites(
                .sendFavourites(
                    FavouriteRequest(imageId: imageId)
                )
            )
        )
        .sink { [weak self] in
            switch $0 {
            case .failure(_):
                self?.isFavourite = false
            case .finished:
                self?.isFavourite = true
            }
        } receiveValue: { [weak self] (favouriteResponse: FavouriteResponse) in
            guard let self else { return }
            self.favourite?.id = favouriteResponse.id
            self.favourite = Favourite(
                id: favouriteResponse.id,
                imageId: imageId
            )
        }
        .store(in: &cancellables)
    }
    
    func deleteFavourite() {
        guard let favourite else {
            isFavourite = true
            return
        }
        
        services.networkingService.request(
            .favourites(.deleteFavourites(String(favourite.id)))
        )
        .sink { [weak self] in
            switch $0 {
            case .failure(_):
                self?.isFavourite = true
            case .finished:
                self?.isFavourite = false
            }
        } receiveValue: { (_: EmptyResponse) in }
            .store(in: &cancellables)
    }
}

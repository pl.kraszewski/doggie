//
//  SceneDelegate.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    // MARK: - Properties
    
    var window: UIWindow?
    var coordinator: Coordinator?
    var appServices: AppServices = .makeAppServices()

    // MARK: - UIWIndowSceneDelegate

    func scene(
        _ scene: UIScene,
        willConnectTo session: UISceneSession,
        options connectionOptions: UIScene.ConnectionOptions
    ) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let navigationController = UINavigationController()
        navigationController.navigationBar.tintColor = Color.brown
        
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        
        coordinator = BreedListCoordinator(
            navigationController: navigationController,
            appServices: appServices
        )
        coordinator?.start()
    }
}

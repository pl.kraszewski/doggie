//
//  ImageDescriptionTableViewCell.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

import UIKit

final class ImageDescriptionTableViewCell: UITableViewCell {
    
    // MARK: Properties
    
    static var identifier: String {
        String(describing: Self.self)
    }
    
    // MARK: - Private Subview
    
    private let backgroundColorView: UIView = {
        let view = UIView()
        view.backgroundColor = Color.transparentWhite
        view.layer.cornerRadius = GlobalConstants.cornerRadius
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let spacerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK: - Subview
    
    let mainImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.layer.cornerRadius = GlobalConstants.cornerRadius
        image.clipsToBounds = true
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    let favouriteImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.image = Symbol.star
        image.tintColor = Color.brown
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = Font.bold20
        label.textColor = Color.brown
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Font.regular15
        label.textColor = Color.defaultDark
        label.numberOfLines = 0
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = .large
        indicator.color = Color.brown
        indicator.translatesAutoresizingMaskIntoConstraints = false
        return indicator
    }()
    
    // MARK: - PrepareForReuse
    
    override func prepareForReuse() {
        mainImage.image = nil
    }
    
    // MARK: - Initializator
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupSubviews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup
    
    private func setupView() {
        selectionStyle = .none
        backgroundColor = .clear
    }
    
    private func setupSubviews() {
        [
            backgroundColorView,
            spacerView,
            mainImage,
            favouriteImage,
            titleLabel,
            descriptionLabel,
            activityIndicator
        ]
            .forEach(addSubview)
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate(
            [
                mainImage.topAnchor.constraint(equalTo: topAnchor),
                mainImage.leadingAnchor.constraint(equalTo: leadingAnchor),
                mainImage.heightAnchor.constraint(equalToConstant: Constants.mainImageHeight),
                mainImage.widthAnchor.constraint(equalTo: mainImage.heightAnchor, multiplier: Constants.mainImageMultiplier),
                
                activityIndicator.centerXAnchor.constraint(equalTo: mainImage.centerXAnchor),
                activityIndicator.centerYAnchor.constraint(equalTo: mainImage.centerYAnchor),
                
                favouriteImage.topAnchor.constraint(equalTo: topAnchor, constant: Margins.tiny),
                favouriteImage.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Margins.tiny),
                favouriteImage.heightAnchor.constraint(equalToConstant: Constants.favouriteImageHeight),
                favouriteImage.widthAnchor.constraint(equalTo: favouriteImage.heightAnchor, multiplier: Constants.favouriteImageMultiplier),
                
                titleLabel.centerYAnchor.constraint(equalTo: mainImage.centerYAnchor),
                titleLabel.leadingAnchor.constraint(equalTo: mainImage.trailingAnchor, constant: Margins.outer),
                titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Margins.outer),
                
                descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Margins.outer),
                descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Margins.outer),
                descriptionLabel.topAnchor.constraint(equalTo: mainImage.bottomAnchor, constant: Margins.tiny),
                
                backgroundColorView.topAnchor.constraint(equalTo: topAnchor),
                backgroundColorView.leadingAnchor.constraint(equalTo: leadingAnchor),
                backgroundColorView.trailingAnchor.constraint(equalTo: trailingAnchor),
                backgroundColorView.bottomAnchor.constraint(equalTo: spacerView.topAnchor),
                
                spacerView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: Margins.tiny),
                spacerView.leadingAnchor.constraint(equalTo: leadingAnchor),
                spacerView.trailingAnchor.constraint(equalTo: trailingAnchor),
                spacerView.heightAnchor.constraint(equalToConstant: Margins.outer),
                spacerView.bottomAnchor.constraint(equalTo: bottomAnchor)
            ]
        )
    }
}

// MARK: - Constants

extension ImageDescriptionTableViewCell {
    enum Constants {
        static let mainImageHeight: CGFloat = 100
        static let mainImageMultiplier: CGFloat = 1.5
        
        static let favouriteImageHeight: CGFloat = 30
        static let favouriteImageMultiplier: CGFloat = 1
    }
}


//
//  DetailsView.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 27/05/2023.
//

import UIKit

final class DetailsView: UIView {
    
    //MARK: - Private Subview
    
    private let breedForPrefixLabel: UILabel = {
        let label = UILabel()
        label.text = R.string.base.breedFor()
        label.font = Font.bold20
        label.textColor = Color.brown
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let breedGroupPrefixLabel: UILabel = {
        let label = UILabel()
        label.text = R.string.base.breedGroup()
        label.font = Font.bold20
        label.textColor = Color.brown
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let lifeSpanPrefixLabel: UILabel = {
        let label = UILabel()
        label.text = R.string.base.lifeSpan()
        label.font = Font.bold20
        label.textColor = Color.brown
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let temperamentPrefixLabel: UILabel = {
        let label = UILabel()
        label.text = R.string.base.temperament()
        label.font = Font.bold20
        label.textColor = Color.brown
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let originPrefixLabel: UILabel = {
        let label = UILabel()
        label.text = R.string.base.origin()
        label.font = Font.bold20
        label.textColor = Color.brown
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    //MARK: - Subview
    
    let breedForInfoLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = Font.regular20
        label.textColor = Color.defaultDark
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let breedGroupInfoLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = Font.regular20
        label.textColor = Color.defaultDark
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    let lifeSpanInfoLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = Font.regular20
        label.textColor = Color.defaultDark
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let originInfoLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = Font.regular20
        label.textColor = Color.defaultDark
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let temperamentInfoLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = Font.regular20
        label.textColor = Color.defaultDark
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    //MARK: - Initializator
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Setup
    
    func setupSubviews() {
        [
            breedForPrefixLabel,
            breedForInfoLabel,
            breedGroupPrefixLabel,
            breedGroupInfoLabel,
            lifeSpanPrefixLabel,
            lifeSpanInfoLabel,
            originPrefixLabel,
            originInfoLabel,
            temperamentPrefixLabel,
            temperamentInfoLabel
        ]
            .forEach(addSubview)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate(
            [
                breedForPrefixLabel.topAnchor.constraint(equalTo: topAnchor, constant: Margins.outer),
                breedForPrefixLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Margins.outer),
                breedForPrefixLabel.widthAnchor.constraint(equalToConstant: Constants.prefixWidth),
                
                breedForInfoLabel.topAnchor.constraint(equalTo: breedForPrefixLabel.topAnchor),
                breedForInfoLabel.leadingAnchor.constraint(equalTo: breedForPrefixLabel.trailingAnchor, constant: Margins.tiny),
                breedForInfoLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Margins.outer),
                
                breedGroupPrefixLabel.topAnchor.constraint(equalTo: breedForInfoLabel.bottomAnchor, constant: Margins.outer),
                breedGroupPrefixLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Margins.outer),
                breedGroupPrefixLabel.widthAnchor.constraint(equalToConstant: Constants.prefixWidth),
                
                breedGroupInfoLabel.topAnchor.constraint(equalTo: breedGroupPrefixLabel.topAnchor),
                breedGroupInfoLabel.leadingAnchor.constraint(equalTo: breedGroupPrefixLabel.trailingAnchor, constant: Margins.tiny),
                breedGroupInfoLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Margins.outer),
                
                lifeSpanPrefixLabel.topAnchor.constraint(equalTo: breedGroupInfoLabel.bottomAnchor, constant: Margins.outer),
                lifeSpanPrefixLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Margins.outer),
                lifeSpanPrefixLabel.widthAnchor.constraint(equalToConstant: Constants.prefixWidth),
                
                lifeSpanInfoLabel.topAnchor.constraint(equalTo: lifeSpanPrefixLabel.topAnchor),
                lifeSpanInfoLabel.leadingAnchor.constraint(equalTo: lifeSpanPrefixLabel.trailingAnchor, constant: Margins.tiny),
                lifeSpanInfoLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Margins.outer),
                
                temperamentPrefixLabel.topAnchor.constraint(equalTo: lifeSpanInfoLabel.bottomAnchor, constant: Margins.outer),
                temperamentPrefixLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Margins.outer),
                temperamentPrefixLabel.widthAnchor.constraint(equalToConstant: Constants.prefixWidth),
                
                temperamentInfoLabel.topAnchor.constraint(equalTo: temperamentPrefixLabel.topAnchor),
                temperamentInfoLabel.leadingAnchor.constraint(equalTo: temperamentPrefixLabel.trailingAnchor, constant: Margins.tiny),
                temperamentInfoLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Margins.outer),
                
                originPrefixLabel.topAnchor.constraint(equalTo: temperamentInfoLabel.bottomAnchor, constant: Margins.outer),
                originPrefixLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Margins.outer),
                originPrefixLabel.widthAnchor.constraint(equalToConstant: Constants.prefixWidth),
                originPrefixLabel.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor, constant: -Margins.outer),
                
                originInfoLabel.topAnchor.constraint(equalTo: originPrefixLabel.topAnchor),
                originInfoLabel.leadingAnchor.constraint(equalTo: originPrefixLabel.trailingAnchor, constant: Margins.tiny),
                originInfoLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Margins.outer),
                originInfoLabel.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor, constant: -Margins.outer)
            ]
        )
    }
}

//MARK: - Constants

extension DetailsView {
    enum Constants {
        static let prefixWidth: CGFloat = 140.0
    }
}

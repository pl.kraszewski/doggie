//
//  URLComponents+.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

import Foundation.NSURL

extension URLComponents {
    mutating func setQueryItems(with parameters: [String: String]) {
        queryItems = parameters.map { URLQueryItem(name: $0.key, value: $0.value) }
    }
}

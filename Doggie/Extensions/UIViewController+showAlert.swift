//
//  UIViewController+showAlert.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 27/05/2023.
//

import UIKit.UIViewController

extension UIViewController {
    func showAlert(title: String, message: String, actions: [UIAlertAction] = []) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for action in actions { alert.addAction(action) }
        present(alert, animated: true)
    }
}


//MARK: - AppAlert

struct AppAlert {
    let title: String
    let message: String
    var actions: [UIAlertAction] = []
}

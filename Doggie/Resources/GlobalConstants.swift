//
//  GlobalConstants.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 27/05/2023.
//

import Foundation

enum GlobalConstants {
    /** 20.0 */
    static let cornerRadius: CGFloat = 20.0
}

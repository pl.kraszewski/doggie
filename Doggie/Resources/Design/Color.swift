//
//  Color.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

import UIKit.UIColor

enum Color {
    static let beige = UIColor(resource: R.color.beige)
    static let brown = UIColor(resource: R.color.brown)
    static let defaultDark = UIColor(resource: R.color.defautDark)
    static let transparentWhite = UIColor(resource: R.color.white)?.withAlphaComponent(0.3)
}

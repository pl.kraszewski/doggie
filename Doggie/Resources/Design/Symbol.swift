//
//  Symbol.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 26/05/2023.
//

import UIKit.UIImage

enum Symbol {
    static let star = UIImage(systemName: "star")
    static let fillStar = UIImage(systemName: "star.fill")
}

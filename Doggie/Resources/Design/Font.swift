//
//  Font.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

import UIKit.UIFont

enum Font {
    static let main60: UIFont? = UIFont(name: "Avenir", size: 60)
    static let main40: UIFont? = UIFont(name: "Avenir", size: 40)
    
    static let regular15: UIFont? = UIFont(name: "Helvetica", size: 15)
    static let regular20: UIFont? = UIFont(name: "Helvetica", size: 20)
    
    static let bold20: UIFont? = UIFont(name: "Helvetica-Bold", size: 20)
}

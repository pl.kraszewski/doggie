//
//  Margins.swift
//  Doggie
//
//  Created by Paweł Kraszewski on 25/05/2023.
//

import Foundation

struct Margins {
    /** 4.0  */
    static let micro: CGFloat = 4.0
    /** 8.0  */
    static let tiny: CGFloat = 8.0
    /** 12.0  */
    static let small: CGFloat = 12.0
    /** 20.0  */
    static let outer: CGFloat = 20.0
    /** 60.0  */
    static let large: CGFloat = 60.0
}

﻿
  

# 🐶 Doggie (iOS app)

  

## 📝 Description

A simple application showcasing various dog breeds, with the ability to check detailed information about the selected breed and mark favorite breeds.  

## 🏛 Architecture

  

  

********MVVM-C******** (Model-View-ViewModel + Coordinator)

  

  

## 🛠 Tech stack

  

  

- ****UIKit****
- ****Dependency injections****
- ****CocaPods**** (R.Swift)
- ****UI from code**** (AutoLayouts)
- ****Combine****
- ****URLSession****

  

## 📈 Prepare for future growth

- Coordinators effectively separate the responsibility for navigation to separate components, which facilitates future flow between a large number of screens.

- AppService is designed to easily add additional services and use them in selected modules, such as UserSessionService or FirebaseService.

- NetworkingService is designed to easily customize it, add new endpoints, different authentication methods, etc.

## 💻 Prerequisites


To successfully run this project, make sure you have CocoaPods installed. If you don't have CocoaPods installed, please install it using the instructions available on the [CocoaPods website](https://cocoapods.org/).

## Author

Paweł Kraszewski
